from django.db import models

# Create your models here.
#Unidade da Federação
class UF(models.Model):
    #Listas

    #Atributos
    nome = models.CharField(max_length=50, verbose_name='Nome:',blank=True)
    sigla = models.CharField(max_length=2, verbose_name='Sigla:',blank=True)
    codigo_do_ibge = models.CharField(max_length=6, verbose_name='Código do IBGE:',blank=True)
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Unidade da Feração'
        verbose_name_plural = 'Undiades da Federação'

    #Representação textual
    def __str__(self):
        return "%s - %s" % (self.sigla,self.nome)


#Unidade da Municipio
class Municipio(models.Model):
    #Listas

    #Atributos do Model
    nome = models.CharField(max_length=100, verbose_name='Nome:',blank=True)
    codigo_do_ibge = models.CharField(max_length=6, verbose_name='Código do IBGE:',blank=True)

    #Atributos de Relacionamentos
    estado = models.ForeignKey('UF', verbose_name='Estado:',on_delete=models.PROTECT,blank=True)
    registroDeCasosSuspeitos
    #registroDeCasosConfirmados
    #registroDeCasosGraves
    #registroDeCasosObitos
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'

    #Representação textual
    def __str__(self):
        return "%s - %s" % (self.estado.sigla ,self.nome)

"""
#Instituições de Saúde
class InstituicaoDeSaude(models.Model):
    #Listas
    TIPO_DE_INSTITUICAO =(
        (0,'publica'),
        (1,'privada'),
    )
    #Atributos do Model
    nome = models.CharField(max_length=200, verbose_name='Nome:')
    tipo 
    endereço
    #Atributos de Relacionamentos
    municipio
    registrosDeRecursos
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Instituição de Saúde'
        verbose_name_plural = 'Instituições de Saúde'

    #Representação textual
    def __str__(self):
        return "%s - %s" % (self.municipio ,self.nome)

#Registo de Casos Suspeitos
class RegistoDeCasosSuspeitos(models.Model):
    #Listas

    #Atributos do Model
    quantidade =
    created
    #Atributos de Relacionamentos
    municipio
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Casos Suspeitos'
        verbose_name_plural = 'Registros de Casos Suspeitos'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

#Registo de Casos Confirmados
class RegistoDeCasosConfirmados(models.Model):
    #Listas

    #Atributos do Model
    quantidade =
    created
    #Atributos de Relacionamentos
    municipio
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Casos Confirmados'
        verbose_name_plural = 'Registros de Casos Confirmados'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

#Registo de Casos Obitos
class RegistoDeCasosObitos(models.Model):
    #Listas

    #Atributos do Model
    quantidade =
    created
    #Atributos de Relacionamentos
    municipio
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Casos Obitos'
        verbose_name_plural = 'Registros de Casos Obitos'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

#Registo de Casos Graves
class RegistoDeCasosGraves(models.Model):
    #Listas

    #Atributos do Model
    quantidade =
    created
    #Atributos de Relacionamentos
    municipio
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Casos Graves'
        verbose_name_plural = 'Registros de Casos Graves'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

#Registo Populacional
class RegistoPopulacional(models.Model):
    #Listas

    #Atributos do Model
    populacao_total
    populacao_grupo_de_risco
    populacao_idosos
    populacao_adultos
    populacao_jovens
    populacao_infantil
    #Atributos de Relacionamentos
    municipio = 
    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Casos Populacional'
        verbose_name_plural = 'Registros de Populacional'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

#Registo de Recursos em Saúde
class RegistoDeRecursosEmSaude(models.Model):
    #Listas
    TIPO_DE_RECURSO = (
        (0,'LEITO COVID-19')
    )
    #Atributos do Model
    tipo
    quantidade
    created
    #Atributos de Relacionamentos

    #Metodos

    #Meta dados
    class Meta:
        verbose_name = 'Registro de Recurso em Saúde'
        verbose_name_plural = 'Registros de Recursos em Saúde'

    #Representação textual
    def __str__(self):
        return "%s - %s / %s" % (self.municipio ,self.quantidade, self.created)

"""
"""
#Modelo de Classe
class ClasseName(models.Model):
    #Listas

    #Atributos

    #Metodos

    #Meta dados
    class Meta:
        verbose_name = ''
        verbose_name_plural = ''

    #Representação textual
    def __str__(self):
        return "%s - %s" % (self.atr, self.atr)
"""


